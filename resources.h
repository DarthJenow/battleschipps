/*
 * Dateiname:			ressources.h
 * Datum:				2021-06-25 01:55:35
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-25 01:55:35
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				Strukturen, welche die Texte beinhaltet, die an mehreren Stellen benötigt werden,
 * 						um bei Veränderungen nur an einer Stelle überarbeiten zu müssen
 */

#pragma once

#include <string>
using namespace std;

// #pragma once hilft hier nicht --> altmodische Art
#ifndef RESOURCES_H
	#define RESOURCES_H

	namespace resources {
		// speichert den Spielverlauf-Dateiname
		const string spielverlaufDateiname = "Spielverlauf.txt";
	};

	// Text für Spielrunden
	namespace spiel {
		const string muenzwurfInit1 = "Ich werfe eine Muenze, um den ersten Spieler zu bestimmen.\nBei Kopf beginnt ";
		const string muenzwurfInit2 = ", bei Zahl beginnt ";
		const string muenzwurfInit3 = ".\nEs ist ";
		const string muenzKopf = "Kopf";
		const string muenzZahl = "Zahl";
		const string muenzWin1 = "! ";
		const string muenzwin2 = " beginnt.\n";

		const string endstellung = "Endstellung";

		// const string spielfeldNameGenitiv = "s Spielfeld:"; // MAYBE REPLACE

		const string win = " hat gewonnen.\n\n";
		const string winSpielfeld = " Spielfeld:";
	};

	// Text für Navigation
	namespace nav {
		const string enterFortfahren = "\nDruecken Sie Enter um fortzufahren.";
		const string enterToMenu = "\nDruecke Enter, um ins Menu zurueck zu kehren.";
	};

	namespace boxDrawingChars {
		#ifdef __linux__
			const string gray = "░";
			const string lgray = "▒";
			const string white = "▓";
			const string ver = "│";
			const string tr = "┤";
			const string trr = "╡";
			const string ttr = "╢";
			const string orr = "╖";
			const string oor = "╕";
			const string ttrr = "╣";
			const string vver = "║";
			const string oorr = "╗";
			const string uurr = "╝";
			const string urr = "╜";
			const string uur = "╛";
			const string ore = "┐";
			const string ul = "└";
			const string tu = "┴";
			const string to = "┬";
			const string tl = "├";
			const string hor = "─";
			const string cross = "┼";
			const string tll = "╞";
			const string ttl = "╟";
			const string uull = "╚";
			const string ooll = "╔";
			const string ttuu = "╩";
			const string ttoo = "╦";
			const string ttll = "╠";
			const string hhor = "═";
			const string ccross = "╬";
			const string ttu = "╧";
			const string tuu = "╨";
			const string tto = "╤";
			const string too = "╥";
			const string ull = "╙";
			const string uul = "╘";
			const string ool = "╒";
			const string oll = "╓";
			const string vcross = "╫";
			const string hcross = "╪";
			const string ur = "┘";
			const string ol = "┌";
		#endif

		#ifdef _WIN32
			const string gray = { char(176) };
			const string lgray = { char(177) };
			const string white = { char(178) };
			const string ver = { char(179) };
			const string tr = { char(180) };
			const string trr = { char(181) };
			const string ttr = { char(182) };
			const string orr = { char(183) };
			const string oor = { char(184) };
			const string ttrr = { char(185) };
			const string vver = { char(186) };
			const string oorr = { char(187) };
			const string uurr = { char(188) };
			const string urr = { char(189) };
			const string uur = { char(190) };
			const string ore = { char(191) };
			const string ul = { char(192) };
			const string tu = { char(193) };
			const string to = { char(194) };
			const string tl = { char(195) };
			const string hor = { char(196) };
			const string cross = { char(197) };
			const string tll = { char(198) };
			const string ttl = { char(199) };
			const string uull = { char(200) };
			const string ooll = { char(201) };
			const string ttuu = { char(202) };
			const string ttoo = { char(203) };
			const string ttll = { char(204) };
			const string hhor = { char(205) };
			const string ccross = { char(206) };
			const string ttu = { char(207) };
			const string tuu = { char(208) };
			const string tto = { char(209) };
			const string too = { char(210) };
			const string ull = { char(211) };
			const string uul = { char(212) };
			const string ool = { char(213) };
			const string oll = { char(214) };
			const string vcross = { char(215) };
			const string hcross = { char(216) };
			const string ur = { char(217) };
			const string ol = { char(218) };
		#endif
	};
#endif