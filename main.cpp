/*
 * Dateiname:			main.cpp
 * Datum:				2021-06-09 12:59:07
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-09 12:59:07
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include "battleschipps.h"
#include "schiffspieler.h"
#include "computergegner.h"
#include "spiellog.h"

#include <ctime>
#include <random>
#include <fstream>
#include <string>
#include <iostream>
using namespace std;

int modusWaehlen();
void spielSingleplayer();
void spielMultiplayer();
void regelnAnzeigen();

int main()
{
	konsoleAnpassen();

	/**
	 * Modus Auswahl
	 */
	int mode;
	bool quit = false;

	do {
		mode = modusWaehlen();

		switch (mode) {
			case 1:
				spielSingleplayer();
				break;
			case 2:
				spielMultiplayer();
				break;
			case 3:
				regelnAnzeigen();
				break;
			case 4:
				quit = true;
				break;
		}
	} while (!quit);

	konsoleLoeschen();

	ifstream asciiArtFile("Outro");
	char fileRead;

	/**
	 * Wenn die Datei nicht geöffnet werden kann, ist es nicht schlimm
	 * Es gibt dann einfach kein Bild
	 * --> Programm muss nicht abgebrochen werden
	 */
	if (asciiArtFile) {
		while (asciiArtFile.get(fileRead)) {
			cout << fileRead;
		}
	}

	asciiArtFile.close();

	cout << "\n\n\nAhoi, ihr Landratten!\n" << endl;

	return 0;
}

// Der Spieler wählt einen Spielmodus aus (1 = SP, 2 = MP, 3 = Regeln, 4 = quit)
int modusWaehlen() {
	string input;
	string message;

	string asciiArt = "";

	ifstream asciiArtFile("Intro");
	char fileRead;

	/**
	 * Wenn die Datei nicht geöffnet werden kann, ist es nicht schlimm
	 * Es gibt dann einfach kein Bild
	 * --> Programm muss nicht abgebrochen werden
	 */
	if (asciiArtFile) {
		while (asciiArtFile.get(fileRead)) {
			asciiArt += fileRead;
		}
	}

	asciiArtFile.close();

	do {
		konsoleLoeschen();

		cout << asciiArt << "\n\nWillkommen bei BattleSchipps!\n" << endl;

		message += "Waehlen Sie einen Modus aus\n" 
					"1) Singleplayer (Spieler gegen Computer)\n"
					"2) Multiplayer (Spieler gegen Spieler)\n"
					"3) Regeln anzeigen\n"
					"4) Spiel beenden";

		cout << message << endl;

		getline(cin, input);

		if (input.length() == 1) { // ein Zeichen wurde eingegeben, potentiell ein richtiges Zeichen
			return input.at(0) - '0'; // '0' abziehen, um Char in Integer zu wandeln
		} else {
			message = "Ungueltige Eingabe\n";
		}
	} while (true);
}

// Spieler-vs-Computer-Spiel
void spielSingleplayer() {
	Spiellog log;
	int schuss[3]; // gibt den Schuss und das Treffer-Ergebnis für den Spiele-Log zurück
	bool aktuellerSpieler; // 0 / false --> Spieler ist aktiv; 1 / true --> Computer ist aktiv
	bool gewonnen = false;
	string spielerName;
	string gewinnerSpielfeldname;
	string * spielfeldGewinner;
	string * spielfeldVerlierer;
	SchiffSpieler * spielerGewinner;
	SchiffSpieler * spielerVerlierer;

	// Spielernamen einlesen
	konsoleLoeschen();
	cout << "Geben Sie ihren Namen ein: ";
	getline(cin, spielerName);
	// spielerName = "Simon"; // DEBUGGING

	// Spieler und Computer initilisieren
	SchiffSpieler spieler = SchiffSpieler(spielerName);
	Computergegner computer = Computergegner();

	// Spiel-Log initilaisieren
	log.setzeSpieler(&spieler, computer.getSchiffSpieler(), false);

	// Schiffe platzieren
	spieler.schiffePlatzieren();

	// Computerschiffe platzieren
	computer.schiffePlatzieren();

	// Start-Aufstellung in Spiel-Log schreiben
	log.schreibeStartaufstellung();

	// Münzwurf
	// Zufallsgenerator erst kurz vor Einsatz initialisieren, damit clock() nicht bei jeder Ausführung eine fast identische Zahl ausgibt --> Bessere Zufallszahlen
	srand(clock());
	konsoleLoeschen();
	aktuellerSpieler = rand() % 2;
	cout << spiel::muenzwurfInit1 << spieler.getName() << spiel::muenzwurfInit2 << "der Computer" << spiel::muenzwurfInit3;

	if (aktuellerSpieler) {
		cout << spiel::muenzZahl << spiel::muenzWin1 << "Der Computer" << spiel::muenzwin2;
	} else {
		cout << spiel::muenzKopf << spiel::muenzWin1 << spieler.getName() << spiel::muenzwin2;
	}

	pause(nav::enterFortfahren);

	// Gameloop
	do {
		if (aktuellerSpieler) { // Computer ist dran
			if (computer.spielzug(&spieler, schuss)) {
				aktuellerSpieler = 1;

				spielerGewinner = computer.getSchiffSpieler();
				spielerVerlierer = &spieler;

				gewonnen = true;
			}
		} else { // Spieler ist dran
			if (spieler.spielzugSingleplayer(computer.getSchiffSpieler(), schuss)) {
				aktuellerSpieler = 0;

				spielerGewinner = &spieler;
				spielerVerlierer = computer.getSchiffSpieler();

				gewonnen = true;
			}
		}

		log.schreibeSpielzug(aktuellerSpieler, schuss);
		aktuellerSpieler ^= 1; // aktiven Spieler tauschen
	} while (!gewonnen);

	/**
	 * Die Runde ist vorbei
	 * Gewinner und die Spielfelder werden ausgegeben
	 * 
	 * am Ende der Schleife wird aktuellerSpieler geflippt
	 * --> momentan ist der Verlierer ausgewählt
	 * --> erneut flippen, damit der Gewinner ausgewählt ist
	 */
	aktuellerSpieler ^= 1;

	if (aktuellerSpieler) {
		spielerGewinner = computer.getSchiffSpieler();
		spielerVerlierer = &spieler;
	} else {
		spielerGewinner = &spieler;
		spielerVerlierer = computer.getSchiffSpieler();
	}

	string space = wiederholeString(" ", spieler.felderAbstand);

	konsoleLoeschen();
	cout << spielerGewinner->getName() << spiel::win;

	spielfeldGewinner = spielerGewinner->getEigenesSpielfeld();
	spielfeldVerlierer = spielerVerlierer->getEigenesSpielfeld();
	gewinnerSpielfeldname = spielerGewinner->getNameGenitiv() + spiel::winSpielfeld;

	cout << gewinnerSpielfeldname << wiederholeSpace(spieler.visuelleBreite - gewinnerSpielfeldname.length() + spieler.felderAbstand) << spielerVerlierer->getNameGenitiv() << spiel::winSpielfeld << endl;

	for (int i = 0; i < spieler.visuelleHoehe; i++) {
		cout << spielfeldGewinner[i] << space << spielfeldVerlierer[i] << endl;
	}

	// Gewinner in Datei schreiben
	log.schreibeGewinner(aktuellerSpieler);

	/**
	 * DEBUGGING
	 * AutoHotKey-Script stoppen
	 */
	// cin.ignore(INT32_MAX, 'x');

	// dynamische Arrays löschen
	delete[] spielfeldGewinner;
	delete[] spielfeldVerlierer;

	pause (nav::enterToMenu);
}

// Spieler-vs-Spieler-Spiel
void spielMultiplayer() {
	Spiellog log;
	int schuss[3]; // gibt den Schuss und das Treffer-Ergebnis für den Spiele-Log zurück
	bool aktuellerSpieler; // 0 / false --> 1. Spieler ist aktiv; 1 / true --> zweiter Spieler ist aktiv
	bool gewonnen = false;
	string spielerName[2];
	string space;
	string gewinnerNameSpielfeld;
	string * spielfeldGewinner;
	string * spielfeldVerlierer;

	/**
	 * Spieler vorbereiten
	 */

	// Spielernamen einlesen
	konsoleLoeschen();
	cout << "Geben Sie einen Namen fuer den ersten Spieler ein: ";
	getline(cin, spielerName[0]);
	// spielerName[0] = "Simon"; // DEBUGGING

	cout << "\nGeben Sie einen Namen fuer den zweiten Spieler ein: ";
	getline(cin, spielerName[1]);
	// spielerName[1] = "Eugen"; // DEBUGGING

	// Array mit den beiden Spielern initialisieren
	SchiffSpieler spieler[2] = {SchiffSpieler(spielerName[0]), SchiffSpieler(spielerName[1])};

	// Spiel-Log initialisieren und Spielernamen schreiben
	log.setzeSpieler(&spieler[0], &spieler[1], true);
	
	// Schiffe platzieren
	for (int i = 0; i < 2; i++) {
		spieler[i].schiffePlatzieren();
	}

	// Startaufstellung in Datei schreiben
	log.schreibeStartaufstellung();

	// Münzwurf, wer das Spiel beginnen darf
	// Zufallsgenerator erst kurz vor Einsatz initialisieren, damit clock() nicht bei jeder Ausführung eine fast identische Zahl ausgibt --> Bessere Zufallszahlen
	srand(clock());
	konsoleLoeschen();
	aktuellerSpieler = rand() % 2;
	cout << spiel::muenzwurfInit1 << spieler[0].getName() << spiel::muenzwurfInit2 << spieler[1].getName() << spiel::muenzwurfInit3;

	if (aktuellerSpieler) {
		cout << spiel::muenzZahl;
	} else {
		cout << spiel::muenzKopf;
	}

	cout << spiel::muenzWin1 << spieler[aktuellerSpieler].getName() << spiel::muenzwin2;

	pause(nav::enterFortfahren);

	/**
	 * Schleifenbedinung führt den Spielzug aus und meldet zurück, ob das Spiel gewonnen wurde.
	 */
	do {
		if (spieler[aktuellerSpieler].spielzugMultiplayer(&(spieler[aktuellerSpieler ^ 1]), schuss)) {
			gewonnen = true;
		}
		log.schreibeSpielzug(aktuellerSpieler, schuss);
		

		aktuellerSpieler ^= 1;
	} while (!gewonnen);

	/**
	 * Die Runde ist vorbei
	 * Gewinner und die Spielfelder werden ausgegeben
	 * 
	 * am Ende der Schleife wird aktuellerSpieler geflippt
	 * --> momentan ist der Verlierer ausgewählt
	 * --> erneut flippen, damit der Gewinner ausgewählt ist
	 */
	aktuellerSpieler ^= 1;

	spielfeldGewinner = spieler[aktuellerSpieler].getEigenesSpielfeld();
	spielfeldVerlierer = spieler[aktuellerSpieler ^ 1].getEigenesSpielfeld();
	space = wiederholeString(" ", spieler[0].felderAbstand);
	gewinnerNameSpielfeld = spieler[aktuellerSpieler].getNameGenitiv() + spiel::winSpielfeld;

	konsoleLoeschen();
	cout << spieler[aktuellerSpieler].getName() << spiel::win;

	cout << gewinnerNameSpielfeld << wiederholeSpace(spieler[0].visuelleBreite - gewinnerNameSpielfeld.length() + spieler[0].felderAbstand) << spieler[aktuellerSpieler ^ 1].getNameGenitiv() << spiel::winSpielfeld << endl;

	for (int i = 0; i < spieler[0].visuelleHoehe; i++) {
		cout << spielfeldGewinner[i] << space << spielfeldVerlierer[i] << endl;
	}

	// Gewinner in Spiel-Log schreiben
	log.schreibeGewinner(aktuellerSpieler);

	// dynamische Arrays löschen
	delete[] spielfeldGewinner;
	delete[] spielfeldVerlierer;

	pause (nav::enterToMenu);
}

// Zeigt die Regeln an
void regelnAnzeigen() {
	konsoleLoeschen();

	ifstream regelnStream("Regeln.txt");
	char c;

	if (regelnStream) {
		while (regelnStream.get(c)) {
			cout << c;
		}
	} else {
		cout << "Datei konnte nicht geoeffnet werden!";
	}

	regelnStream.close();

	pause("\n\nDruecke Enter um zum Menu zurueck zu kehren.");

	konsoleLoeschen();
}