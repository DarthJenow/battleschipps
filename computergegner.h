/*
 * Dateiname:			computergegner.h
 * Datum:				2021-06-22 08:31:27
 * Autor:				Simon Ziegler@skywalker (zisi1013@h-ka.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-22 08:31:27
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 */

#pragma once

#include "battleschipps.h"
#include "schiffspieler.h"

// Computergegner, welcher mit einer Taktitk Battleschipps spielt
class Computergegner {
	public:
		Computergegner();
		Computergegner(const string computerName);

		string getName();
		SchiffSpieler * getSchiffSpieler();

		void schiffePlatzieren();
		bool spielzug(SchiffSpieler * gegner, int * schuss);
	private:
		SchiffSpieler * spieler;

		// speichert, wie Wahrscheinlich (in einer leeren Startaufstellung) in einer gegebenen Zelle ein Schiff liegt (gemappt auf 0 bis 1000 )
		const int heatmap[10][10] = {{	300, 336, 348, 346, 338, 338, 346, 348, 336, 300	},
									{	336, 263, 278, 270, 261, 261, 270, 278, 263, 336	},
									{	348, 278, 297, 292, 283, 283, 292, 297, 278, 348	},
									{	346, 270, 292, 291, 283, 283, 291, 292, 270, 346	},
									{	338, 261, 283, 283, 276, 276, 283, 283, 261, 338	},
									{	338, 261, 283, 283, 276, 276, 283, 283, 261, 338	},
									{	346, 270, 292, 291, 283, 283, 291, 292, 270, 346	},
									{	348, 278, 297, 292, 283, 283, 292, 297, 278, 348	},
									{	336, 263, 278, 270, 261, 261, 270, 278, 263, 336	},
									{	300, 336, 348, 346, 338, 338, 346, 348, 336, 300	}};


		int heatmapKumuliert[10][10] = { 0 }; // speichert die kumulierte Verteilung der Heatmap; wird beim Initialisieren geschreiben
		bool schussDirs[4] = { 0 }; // speichert für alle 4 Schussrichtungen, ob in diese noch geschossen werden muss (0 = Osten, 3 = Norden)
		int schussKoord[2]; // speichert die ursprünglichen koordinanten des aktuell beschossenen Schiffs

		void init(const string computerName);
		void initRandom();
		void initRandom(int start);

		void erstelleHeatmapKumuliert();
};