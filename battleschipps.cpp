/*
 * Dateiname:			battleships.cpp
 * Datum:				2021-06-10 01:06:51
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-10 01:06:51
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include "battleschipps.h"
#include "schiffspieler.h"

// erstellt einen Platzhalter-String aus Spaces
string wiederholeSpace(int n) {
	return wiederholeString(" ", n);
}

// erstellt einen String mit der Breite einer SchiffSpieler Zelle
string wiederholeString(const string c) {
	SchiffSpieler temp;

	return wiederholeString(c, temp.zelleBreite);
}

// erstellt einen String mit n-Wiederholungen des c-Strings
string wiederholeString(const string c, int n) {
	string temp = "";

	for (int i = 0; i < n; i++) {
		temp += c;
	}

	return temp;
}


// löscht alle Ausgegebenen Zeilen in der Konsole
void konsoleLoeschen() {
	#ifdef _WIN32
		system("CLS");
	#endif
	#ifdef __linux__
		system("clear");
	#endif

	cout << '\n';
}

// wartet auf eine Benutzerbestätigung
void pause(const string message) {
	cout << message << endl;

	string temp;

	getline(cin, temp);
}

// fordert den Spieler auf, das Konsolenfenster anzupassen
void konsoleAnpassen() {
	SchiffSpieler demo; // Demospieler, um ein Spielfeld anzeigen zu lassen
	string input;

	do {
		konsoleLoeschen();

		demo.spielfeldAusgeben("Des Gegner");

		cout << "\nVergroessern Sie ihr Fenster soweit, dass sie beide Spielfelder sehen koennen.\n\nDruecke Enter, um das Feld erneut anzuzeigen. Gebe Sie ein beliebiges Zeichen ein, wenn das Fenster gross genug eingestellt wurde." << endl;

		getline(cin, input);
	} while (input.length() == 0);

	konsoleLoeschen();
}