/*
 * Dateiname:			battleships.h
 * Datum:				2021-06-10 01:06:42
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-10 01:06:42
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 */

#pragma once

#include "resources.h"
#include "schiffspieler.h"

#include <fstream>
#include <iostream>
#include <string>
using namespace std;

/**
 * Allgemeine Funktionen die benötigt werden
 */
string wiederholeSpace(int n);
string wiederholeString(const string c);
string wiederholeString(const string c, int n);

void konsoleLoeschen();
void pause(const string message);
void konsoleAnpassen();