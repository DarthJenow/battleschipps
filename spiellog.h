/*
 * Dateiname:			spiellog.h
 * Datum:				2021-06-25 05:15:39
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-25 05:15:39
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 */

#pragma once

#include "schiffspieler.h"
#include "resources.h"

#include <fstream>
#include <string>
using namespace std;

class Spiellog {
	public:
		Spiellog();
		Spiellog(const string _dateiname);

		void setzeSpieler(SchiffSpieler * _spieler1, SchiffSpieler * _spieler2, bool _multiplayer);

		void schreibeStartaufstellung();
		void schreibeSpielfeld(const string message, SchiffSpieler * spieler);
		void schreibeSpielzug(bool spieler, const int * schuss);
		void schreibeGewinner(bool gewinner);
	private:
		string dateiname = "";

		ofstream logDatei;

		SchiffSpieler * spieler1;
		SchiffSpieler * spieler2;
		
		bool multiplayer;

		void init(const string _dateiname);
		
		bool logOpen();
		bool logOpen(bool anhaengen);
		bool logOpenNoAppend();
		void logClose();
		
		void logSchreiben(const string message);
		void logSchreiben(const string * message, int laenge);
};