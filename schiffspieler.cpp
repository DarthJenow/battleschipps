/*
 * Dateiname:			schiffspieler.cpp
 * Datum:				2021-06-22 08:38:45
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-22 08:38:45
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include "schiffspieler.h"
#include "battleschipps.h"

#include <iostream>
using namespace std;

/**
 * public Methoden
 */
// Standardkonstruktor, nutzt "Spieler" als Spielername
SchiffSpieler::SchiffSpieler() {
	spielerInit("Spieler", "Des Spielers");
}

// Konstruktor mit Spielername
SchiffSpieler::SchiffSpieler(const string name) {
	spielerInit(name, name + "s");
}

// Konstruktur mit Spielername und Spielername im Genitiv
SchiffSpieler::SchiffSpieler(const string name, const string nameGenitiv) {
	spielerInit(name, nameGenitiv);
}


// gibt den Namen des Spielers zurück
string SchiffSpieler::getName() {
	return spielerName;
}

// gibt den Namen des Spielers im Genitiv zurück
string SchiffSpieler::getNameGenitiv() {
	return spielerNameGenitiv;
}

// gibt das eigene Spielfeld aus
string * SchiffSpieler::getEigenesSpielfeld() {
	return erstelleSpielfeld(feldDaten);
}

// gibt das eigene Spielfeld ohne Box-Drawing-Chars aus
string * SchiffSpieler::getEigenesSpielfeldSimple() {
	string * feldString = new string[feldHoehe + 1];

	// erstellt die Spaltennummerierung
	feldString[0] = "  ";
	for (int i = 0; i < feldBreite; i++) {
		feldString[0] += " ";
		feldString[0] += ((char) ('A' + i)); // char einzeln hinzufügen, damit er beim addieren in string umgewandelt wird
	}

	// erstellt die Spielfeldreihen
	for (int i = 1; i <= feldHoehe; i++) {
		// wenn die Zeilennummer einstellig ist, eine Leerstelle einfügen
		if (i < feldHoehe) {
			feldString[i] = " " + to_string(i);
		} else {
			/**
			 * Visual Studio zeigt hier eine Warnung an, dass i größer werden könnte, als es Felder in feldString gibt
			 * Durch die for-Bedinung ist aber sichergestellt, dass dies nicht der Fall ist.
			 * Auch wenn der Typ von i zu bool gewechselt wird, und dadurch sicher im Wertebereich von feldString liegt (ist natürlich nicht nutzbar),
			 * wird die Warnung weiterhin angezeigt.
			 * Laut einiger Google-Einträge scheint dies ein Bug in der Code-Analyse von Visual Studio zu sein.
			 */
			feldString[i] = to_string(i);
		}

		for (int j = 0; j < feldBreite; j++) {
			feldString[i] += " ";
			feldString[i] += data2char(feldDaten[i - 1][j]);
		}
	}

	return feldString;
}

// gibt das feldDaten-Array aus (als String-Array, da gleiches Handling wie char-Array, aber nur 1-Dimensional)
string * SchiffSpieler::getFeldDaten() {
	string * temp = new string[feldHoehe];
	string tempString;

	for (int i = 0; i < feldHoehe; i++) {
		tempString = "";

		for (int j = 0; j < feldBreite; j++) {
			tempString += feldDaten[i][j];
		}

		temp[i] = tempString;
	}
	
	return temp;
}

// gibt das gegnerDaten-Array aus (als String-Array, da gleiches Handling wie char-Array, aber nur 1-Dimensional)
string * SchiffSpieler::getGegnerDaten() {
	string * temp = new string[feldHoehe];
	string tempstring;

	for (int z = 0; z < feldHoehe; z++) {
		tempstring = "";
		
		for (int s = 0; s < feldBreite; s++) {
			tempstring += gegnerDaten[z][s];
		}

		temp[z] = tempstring;
	}

	return temp;
}

// gibt ein Array mit der Anzahl der Schiffe zurück
int * SchiffSpieler::getAnzahlSchiffe() {
	return anzahlSchiffe;
}


// Gibt das Spielfeld in der Konsole aus
void SchiffSpieler::spielfeldAusgeben(string gegnerName) {
	spielfeldAusgeben("", gegnerName, true);
}

// Gibt das Spielfeld mit einem Titel davor aus
void SchiffSpieler::spielfeldAusgeben(string titel, string gegnerName) {
	spielfeldAusgeben(titel, gegnerName, true);
}

void SchiffSpieler::spielfeldAusgeben(string titel, string gegnerName, bool gebeGegnerAus) {
	// Wenn ein Titel übergeben wurde, gebe diesen aus
	if (titel != "") {
		cout << titel << "\n\n";
	}

	string * visual = erstelleSpielfeld(feldDaten);
	string * gegner = erstelleSpielfeld(gegnerDaten);

	// Gegnerspielfed soll mit ausgegeben werden --> Spielfelder sollen beschriftet werden
	if (gebeGegnerAus) {
		string meinFeldName = "Deine Stellung";
		cout << meinFeldName << wiederholeSpace(visuelleBreite - meinFeldName.length() + felderAbstand) << gegnerName << "s Stellung" << endl;
	}

	for (int i = 0; i < visuelleHoehe; i++) {
		cout << visual[i];

		if (gebeGegnerAus) {
			cout  << wiederholeSpace(felderAbstand) << gegner[i];
		}

		cout << endl;
	}

	// dynamische Arrays löschen
	delete[] visual;
	delete[] gegner;
}

// gibt das eigene Spielfeld aus
void SchiffSpieler::meinSpielfeldAusgeben(string titel) {
	spielfeldAusgeben(titel, "", false);
}


// Gibt die Schiffslänge des Typs zurück
int SchiffSpieler::typ2SchiffLaenge(char typ) {
	switch (typ) {
		case 'S':
		case 's':
			return 5;
		case 'K':
		case 'k':
			return 4;
		case 'Z':
		case 'z':
			return 3;
		case 'U':
		case 'u':
			return 2;
		default:
			return -1;
	}
}

// Gibt den Schifftyp der übergebenen Länge zurück
char SchiffSpieler::schiffLaenge2Typ(int len) {
	switch (len) {
		case 2:
			return 'U';
		case 3:
			return 'Z';
		case 4:
			return 'K';
		case 5:
			return 'S';
		default:
			return '0';
	}
}

// erstellt einen String aus einem Koordinatenarray
string SchiffSpieler::koord2String(const int * koordinaten) {
	return koord2String(koordinaten[0], koordinaten[1]);
}

// erstellt einen String aus Zeilen und Spalten Koordinaten
string SchiffSpieler::koord2String(int z, int s) {
	string koordString = "";
	koordString += char(s + 'A') + to_string(z + 1);
	return koordString;
}


// setzt ein Schiff auf die übergebene Koordinate. Gibt false zurück, wenn setzen nicht möglich
bool SchiffSpieler::schiffSetzen(int z, int s, int orientierung, char typ) {
	return schiffSetzen(z, s, orientierung, typ, false);
}

// setzt ein Schiff auf die übergebene Koordinate. orientierung: 0 = Osten, 3 = Norden Gibt false zurück, wenn setzen nicht möglich
bool SchiffSpieler::schiffSetzen(int z, int s, int orientierung, char typ, bool testen) {
	int len = typ2SchiffLaenge(typ);
	bool vert = orientierung % 2 == 1; // gibt an, ob das Schiff vertikal ausgerichtet ist
	bool rueck = orientierung >= 2; // gibt an, ob das Schiff "rückwärts" (unten nach oben / rechts nach links) verläuft

	int koordinate; // speichert die Start-Koordinate, die sich über die Schiffzellen hinweg ändert
	int maxKoord; // speichert den maximalen Wert, der die "wandernde" Zelle haben darf
	int zz, ss; // temporäre genutzte Variablen, um die aktuell geprüfte Zelle anzusprechen
	if (vert) {
		koordinate = z;
		maxKoord = feldHoehe;
	} else {
		koordinate = s;
		maxKoord = feldBreite;
	}

	// überprüfen, ob das Schiff auf das Feld passt
	if (rueck) {
		if (koordinate - len + 1 < 0) {
			return false;
		}
	} else if (koordinate + len > maxKoord) {
		return false;
	}

	/**
	 * Das Schiff passt auf das Feld (hängt nicht über)
	 * --> Falls das Schiff rückwärts verläuft, umrechnen in forwärts
	 */

	if (rueck) {
		koordinate -= len - 1;
	}

	for (int c = koordinate - 1; c < koordinate + len + 1; c++) { // läuft an dem Schiff entlang
		for (int o = -1; o <= 1; o++) { // testet die drei momentanen Felder ab (Schiff selbst und Nachbarn)
			if (vert) {
				zz = c;
				ss = s + o;
			} else {
				zz = z + o;
				ss = c;
			}

			if (!istWasser(zz, ss)) {
				return false;
			}
		}
	}

	// Wenn testen = false übergeben wurde, das Schiff auch in feldDaten schreiben
	if (!testen) {
		// Schiff in Daten-Array schreiben
		for (int c = koordinate; c < koordinate + len; c++) {
			if (vert) {
				zz = c;
				ss = s;
			} else {
				zz = z;
				ss = c;
			}

			feldDaten[zz][ss] = typ;
		}
	}

	return true;
}

// schießt auf Gegner, gibt zurück, ob Treffer (1), Treffer versenkt (2), Wasser (0), fehler (-1)
int SchiffSpieler::schiesseAufGegner(SchiffSpieler * gegner, int z, int s) {
	if (gegnerDaten[z][s] == '0') { // Zelle wurde noch nicht beschossen
		int result = gegner->zelleBeschiessen(z, s);
		
		if (result != 0) {
			gegnerDaten[z][s] = 'x';
		} else {
			gegnerDaten[z][s] = 'w';
		}

		return result;
	} else {
		return -1;
	}
}

// setzt die Daten-Arrays feldDaten und gegnerDaten zurück
void SchiffSpieler::felderReset() {
	// erstellt Standardspielfeld voller Wasser
	for (int i = 0; i < feldHoehe; i++) {
		for (int j = 0; j < feldBreite; j++) {
			feldDaten[i][j] = 'W';
		}
	}

	// initilisiere gegnerDaten mit "noch nicht beschossen"
	for (int i = 0; i < feldHoehe; i++) {
		for (int j = 0; j < feldBreite; j++) {
			gegnerDaten[i][j] = '0';
		}
	}
}


// lässt den Spieler die Schiffe platzieren
void SchiffSpieler::schiffePlatzieren() {
	/**
	 * Durch die Compiler-Optimisierung wird, sobald eine Funtktion false zurückgibt die weiteren Funktionen nicht mehr aufgerufen,
	 * da das Ergebnis des boolschen-Ausdrucks durch die "&&" bereits feststeht.
	 * Dadurch wird sobald das erste mal false (kein Platz mehr) zurückgemeldet wird, direkt die while-Schleife betreten
	 */
	while (!(schiffTypPlatzieren("Schlachtschiff", 'S', anzahlSchiffe[0]) && schiffTypPlatzieren("Kreuzer", 'K', anzahlSchiffe[1]) && schiffTypPlatzieren("Zerstoerer", 'Z', anzahlSchiffe[2]) && schiffTypPlatzieren("U-Boot", 'U', anzahlSchiffe[3]))) {
		konsoleLoeschen();

		meinSpielfeldAusgeben("Du hast dich festgefahren und keinen Platz mehr fuer deine restlichen Schiffe!");

		pause("\nDruecke Enter um von vorne zu beginnen");

		felderReset();
	}

	// gesetzte Stellung anzeigen
	konsoleLoeschen();
	meinSpielfeldAusgeben("Das ist deine Stellung, " + getName() + ":");

	pause("\nDruecke Enter um fortzufahren");
}

// führt einen Singleplayer-Spielzug aus; gibt zurück, ob der Spieler gewonnen hat
bool SchiffSpieler::spielzugSingleplayer(SchiffSpieler * gegner, int * schuss) {
	return spielzug(gegner, false, schuss);
}

// führt einen Multiplayer-Spielzug aus; gibt zurück, ob der Spieler gewonnen hat
bool SchiffSpieler::spielzugMultiplayer(SchiffSpieler * gegner, int * schuss) {
	return spielzug(gegner, true, schuss);
}

// Gibt zurück, ob der Spieler gewonnen hat (--> Gegner hat verloren)
bool SchiffSpieler::pruefeGewonnen(SchiffSpieler * gegner) {
	return gegner->pruefeVerloren();
}


/**
 * private Methoden
 */
void SchiffSpieler::spielerInit(string name, string nameGenitiv) {
	spielerName = name;
	spielerNameGenitiv = nameGenitiv;

	felderReset();
}


// erstellt ein String-Array, welches das Spielfeld visuell darstellt
string * SchiffSpieler::erstelleSpielfeld(const char daten[10][10]) {
	int row = 0; // zählt die aktive Reihe
	string * feldVisuell = new string[visuelleHoehe];

	// erstellt oberen Rand
	feldVisuell[row] = erstelleReihe(ooll, oorr, ttoo, tto, hhor, hhor);
	row++;

	// erstellt Spaltennummerierung mit Buchstaben
	for (int i = 0; i < zelleHoehe; i++) {
		if (i == zelleHoehe / 2) { // Mittig die Spaltennummerierung
			feldVisuell[row] = erstelleCharReihe(vver, vver, vver, ver);
		} else {
			feldVisuell[row] = erstelleReihe(vver, vver, vver, ver, " ", " ");
		}
		
		row++;
	}

	// erstellt Reihennummerierung mit Zahlen und Unterteiler
	for (int i = 0; i < feldHoehe; i++) {
		// Unterteiler erstellen
		if (i == 0) { // Die erste Reihe braucht einen dickeren Rand
			feldVisuell[row] = erstelleReihe(ttll, ttrr, ccross, hcross, hhor, hhor);
		} else { // alle weiteren Reihen haben einen einfachen Rand
			feldVisuell[row] = erstelleReihe(ttl, ttr, vcross, cross, hor, hor);
		}
		row++;

		// eigentliche Reihe erstellen
		for (int j = 0; j < zelleHoehe; j++) {
			if (j == zelleHoehe / 2) { // Mittig die Zeilennummerierung
				feldVisuell[row] = erstelleNumReihe(vver, vver, vver, ver, i + 1, daten[i]);
			} else {
				feldVisuell[row] = erstelleReihe(vver, vver, vver, ver, daten[i]);
			}

			row++;
		}
	}

	// erstellt den unteren Rand
	/**
	 * Visual Studio zeigt hier eine Warnung an, dass row größer werden könnte, als es Felder in feldVisuell gibt
	 * Durch den Code ist aber sichergestellt, dass dies nicht der Fall ist.
	 * Auch wenn der Typ von row zu bool gewechselt wird, und dadurch sicher im Wertebereich von feldVisuell liegt (ist natürlich nicht nutzbar),
	 * wird die Warnung weiterhin angezeigt.
	 * Laut einiger Google-Einträge scheint dies ein Bug in der Code-Analyse von Visual Studio zu sein.
	 */
	feldVisuell[row] = erstelleReihe(uull, uurr, ttuu, ttu, hhor, hhor);

	return feldVisuell;
}

string SchiffSpieler::erstelleReihe(const string bl, const string br, const string c1, const string c, const char * data) {
	string temp = bl;

	// die erste Reihe ist 
	temp += wiederholeString(" ");

	// erstelle die Spielfeldspalten
	for (int i = 0; i < feldBreite; i++) {
		if (i == 0) { // die erste Zelle verwendet einen anderen Seperator
			temp += c1;
		} else {
			temp += c;
		}

		// Der Zelleninhalt anhand der Spielfelddaten erstellen
		temp += " " + wiederholeString(data2icon(data[i]), zelleBreite - 2) + " ";
	}

	temp += br;

	return temp;
}

string SchiffSpieler::erstelleReihe(string bl, string br, string c1, string c, string s1, string s) {
	string temp = bl;

	// erste Zelle mit Platzhalter
	temp += wiederholeString(s1);
	temp += c1;

	// erstelle die Datenfelder
	for (int i = 1; i < feldBreite; i++) {
		temp += wiederholeString(s);
		temp += c;
	}

	temp += wiederholeString(s);
	temp += br;

	return temp;
}

// erstellt eine (visuelle) Spielfeldreihe mit Spaltennummerierung
string SchiffSpieler::erstelleCharReihe(string bl, string br, string c1, string c) {
	string temp = bl;

	 // erstellt die erste, leere Zelle
	temp += wiederholeString(" ");

	// erstellt die restlichen Zellen mit den Buchstaben
	for (char n = 'A'; n < 'A' + feldBreite; n++) {
		if (n == 'A') { // Bei der ersten Spalte wird ein anderer Seperator verwendet
			temp += c1;
		} else {
			temp += c;
		}

		// erstellt den Inhalt der Zelle
		for (int i = 0; i < zelleBreite; i++) {
			if (i == zelleBreite / 2) { // In die Mitte kommt der Buchstabe
				temp += n;
			} else {
				temp += " ";
			}
		}
	}

	temp += br;

	return temp;
}

string SchiffSpieler::erstelleNumReihe(const string bl, const string br, const string c1, const string c, int n, const char * data) {
	string temp = bl;
	
	int offset = 0; // offset für zweistellige Zeilennummer

	if (n >= 10) {
		offset = 1;
	}

	// erstelle die erste Spalte mit den Nummerierungen
	for (int i = offset; i < zelleBreite; i++) {
		if (i == zelleBreite / 2) {
			temp += to_string(n);
		} else {
			temp += " ";
		}
	}

	// erstelle die Spielfeldspalten
	for (int i = 0; i < feldBreite; i++) {
		if (i == 0) {
			temp += c1;
		} else {
			temp += c;
		}

		temp += " " + wiederholeString(data2icon(data[i]), zelleBreite - 2) + " ";
	}

	temp += br;

	return temp;
}


// führt einen Spielzug aus; gibt zurück, ob der Spieler gewonnen hat
bool SchiffSpieler::spielzug(SchiffSpieler * gegner, bool multiplayer, int * schuss) {
	int * koord = new int[2];
	string message;
	string spielfeldMessage = spielerName + " ist am Zug";
	int schussResult = 0;
	
	// koord für do-while-Schleife initialsieieren
	koord[0] = 0;
	
	// Wenn multiplayer: Übergangsbildschirm, damit der andere Spieler sich an den PC setzen kann
	if (multiplayer) {
		konsoleLoeschen();
		pause(spielerName + ", du bist dran. Druecke Enter, um deinen Zug zu beginnen.");
	}

	// Zielkoordinaten eingeben
	do {
		konsoleLoeschen();

		// Der Spieler sitzt am PC, Spielfeld wird angezeigt
		spielfeldAusgeben(spielfeldMessage, gegner->getName());

		if (koord[0] == -1) {
			cout << "\nUngueltige Eingabe!";
		}

		if (schussResult == -1) {
			cout << "\nSie haben bereits auf diese Koordinaten geschossen. Wählen Sie einen neuen Ort!";
		}

		cout << "\nWohin moechtest du schiessen? " << endl;

		koord = getValidKoordEingabe();

		if (koord[0] != -1) {
			schussResult = schiesseAufGegner(gegner, koord[0], koord[1]);	
		}
	} while (koord[0] == -1 || schussResult == -1); // solange Koordinaten eingeben, bis gültige Koordinaten eingegeben wurden, welche noch nicht beschossen wurden

	switch (schussResult) {
		case 0:
			message = "Daneben!";
			break;
		case 1:
			message = "Treffer!";
			break;
		case 2:
			message = "Treffer versenkt!";
			break;
		default:
			// sollte im Normalfall nicht aufgerufen werden
			message = "ERROR";
			break;
	}

	konsoleLoeschen();
	spielfeldAusgeben(spielfeldMessage, gegner->getName());

	cout << '\n' << message << endl;

	pause("Druecken Sie Enter, um ihren Spielzug zu beenden.");

	schuss[0] = koord[0];
	schuss[1] = koord[1];
	schuss[2] = schussResult;

	// dynamische Arrays löschen
	delete[] koord;

	return gegner->pruefeVerloren();
}

// platziert interaktiv anzahl Schiffe desselben Schifftyps
bool SchiffSpieler::schiffTypPlatzieren(string name, char typ, int anzahl) {
	bool valid;
	int * koord = new int[2];
	string input;
	char richtung;
	int orientierung;
	int schiffLaenge = typ2SchiffLaenge(typ);
	string message = "";
	string platzierenMessage = spielerName + ", platziere deine Schiffe!";

	// koord für do-while-Schleife initialsieieren
	koord[0] = 0;

	for (int i = 0; i < anzahl; i++) { // füge anzahl Schiffe hinzu
		if (!pruefePlatz(schiffLaenge)) {
			return false;
		}
		
		do {		
			// Koordinaten des Schiffs abfragen
			do {
				konsoleLoeschen(); // Konsole freimachen

				meinSpielfeldAusgeben(platzierenMessage);

				if (koord[0] == -1) { // es gab bereits eine falsche Eingabe
					cout << "\nUngueltige Koordinateneingabe!";
				}

				// Koordinaten einlesen
				cout << message << "\nGeben Sie die Koordinaten fuer ihr " + to_string(i + 1) + ". " + name + " (Laenge " + to_string(schiffLaenge) + ") in der Form \"A8\" ein: ";

				koord = getValidKoordEingabe();
			} while (koord[0] == -1); // falls Koordinaten nicht gültig waren, nochmal eingebenx

			// Orientierung des Schiffs abfragen
			do {
				konsoleLoeschen();
				meinSpielfeldAusgeben(platzierenMessage);
				
				cout << message << "\nIn Welche Richtung soll das Schiff fahren? (Position: " << koord2String(koord) << ")\nN) Norden\nO) Osten\nS) Sueden\nW) Westen\n";
				
				getline(cin, input);

				// Überprüfen, ob genau ein Zeichen eingegebn wurde
				if (input.length() == 1) {
					richtung = input[0];
				} else {
					/**
					 * Falsche Anzahl Zeichen wurde eingegeben
					 * --> ungültiges Zeichen in richtung speichern, damit Fehler ausgelöst wird
					 */
					richtung = ' ';
				}

				// wandelt die eingegeben Richtung in einen orientierungs-integer für die schiffSetzen-Funktion um				
				switch (richtung) {
					case 'O':
					case 'o':
						orientierung = 0;
						break;
					case 'S':
					case 's':
						orientierung = 1;
						break;
					case 'W':
					case 'w':
						orientierung = 2;
						break;
					case 'N':
					case 'n':
						orientierung = 3;
						break;
					default:
						orientierung = -1;
						break;
				}

				// Fehlermeldung für falsche Orientierung abfangen
				if (orientierung == -1) {
					message = "\nUngueltige Eingabe!";
				} else {
					message = "";
				}

			} while (orientierung == -1); // solange, bis gültige Koordinaten eingegeben wurden

			valid = schiffSetzen(koord[0], koord[1], orientierung, typ); // Schiff an eingegebene Koordinaten platzieren

			// Bei falscher Eingabe fehlermeldung anziegen
			if (!valid) {
				konsoleLoeschen();
				meinSpielfeldAusgeben(platzierenMessage);

				message = "\nSchiff kann nicht platziert werden. Versuche es erneut.\n"; // BUGFIXING
			} else {
				message = "";
			}

		} while (!valid); // solange abfragen, bis an den Koordinaten ein Schiff platziert werden kann
	}

	delete[] koord;

	return true;
}

// schießt auf eine Zelle, gibt zurück, wenn getroffen wurde und speichert treffer intern (0 : daneben; 1 : Treffer; 2 : Treffer versenkt)
int SchiffSpieler::zelleBeschiessen(int z, int s) {
	// Zelle in Kleinbuchstaben wandeln --> Indikator für bereits beschossen
	feldDaten[z][s] += 32;
	
	if (feldDaten[z][s] == 'W' || feldDaten[z][s] == 'w') { // Wasser wurde getroffen
		return 0;
	} else { // Zelle ist kein Wasser --> Zelle ist Schiff
		if (istVersenkt(z, s)) {
			return 2;
		} else {
			return 1;
		}
	}
}


// prüft, ob ein Schiff mit laenge noch auf das Spielfeld passt
bool SchiffSpieler::pruefePlatz(int laenge) {
	// Spielfeld ablaufen und alle möglichen Positionen zählen
	for (int z = 0; z < feldHoehe; z++) {
		for (int s = 0; s < feldHoehe; s++) {
			// überprüft alle 4 Richtungen
			for (int o = 0; o < 4; o++) {
				if (schiffSetzen(z, s, o, schiffLaenge2Typ(laenge), true)) {
					// wenn mindestens ein Schiff gefunden wurde, kann die Suche beendet werden
					return true;
				}
			}
		}
	}

	return false;
}
// überprüft, ob der Spieler verloren hat
bool SchiffSpieler::pruefeVerloren() {
	for (int i = 0; i < feldBreite; i++) {
		for (int j = 0; j < feldHoehe; j++) {
			if (feldDaten[i][j] >= 'A' && feldDaten[i][j] <= 'Z' && feldDaten[i][j] != 'W') { // Großbuchstaben (aber kein 'W') in feldDaten vorhanden --> Schiff wurde noch nicht versenkt --> noch nicht verloren
				return false;
			}
		}
	}

	return true;
}


// übersetzt Daten-char des Spielfelds in visuellen String
string SchiffSpieler::data2icon(char data) {
	switch (data) {
		case 'S':
		case 'K':
		case 'Z':
		case 'U':
			return white;
		case 's':
		case 'k':
		case 'z':
		case 'u':
			return gray;
		case 'w':
			return "-";
		case 'x':
			return "X";
		case 'W':
		case '0':
		default:
			return " ";
	}
}

// übersetzt Daten-char des Spielfelds in visuellen char (ohne Unicode)
char SchiffSpieler::data2char(char data) {
	switch (data) {
		case 'W':
			return ' ';
		case 'S':
		case 'K':
		case 'Z':
		case 'U':
			return '0';
		case 's':
		case 'k':
		case 'z':
		case 'u':
		case 'x':
			return 'X';
		case 'w':
		case '0':
		default:
			return '~';
	}
}


// Lest Koordinaten solange ein, bis sie gültig sind, gitbt {-1, -1} zurück, falls die Eingabe nicht erfolgreich war
int * SchiffSpieler::getValidKoordEingabe() {
	string input;
	char s;
	int z;
	int * koord = new int[2];
	
	// koord mit Fehler-Koordinaten initialisieren
	koord[0] = -1;
	koord[1] = -1;

	getline(cin, input);

	// input muss 2 oder 3 Zeichen groß sein, damit er valide ist
	if (input.length() == 2 || input.length() == 3) {
		// eingegebene Werte extrahieren
		s = input[0];
		z = input[1] - '0';
	
		// wenn 3 Input Stellen --> 2 stellige Zeilennummer
		if (input.length() == 3) {
			z *= 10 + input[2] - '0';
		}

		// Wenn die Spalte in Kleinbuchstaben eingegeben wurden, diese in Großbuchstaben umrechnen
		if (s >= 'a' && s <= 'j') {
			s += 'A' - 'a';
		}
		// überprüfen, ob gültige Koordinaten eingegeben wurden
		if (s >= 'A' && s <= 'J' && z >= 1 && z <= 10) {
			koord[0] = z - 1;
			koord[1] = s - 'A';
		}
	}

	return koord;
}


// überprüft eine Zelle, ob dort ein Schiff platziert ist
bool SchiffSpieler::istWasser(int z, int s) {
	if (z < 0) { // Zelle liegt oberhlab des Spielfelds --> auf erste Zeile korrigieren
		z = 0;
	} else if (z >= feldHoehe) { // Zelle liegt unterhalb des Spielfelds --> auf letzte Zeile korrigieren
		z = feldBreite - 1;
	}

	if (s < 0) { // Zelle liegt links des Spielfelds --> auf erste Spalte korrigieren
		s = 0;
	} else if (s >= feldBreite) { // Zelle liegt rechts des Spielfelds --> auf letzte Spalte korrigieren
		s = feldBreite - 1;
	}

	return feldDaten[z][s] == 'W' || feldDaten[z][s] == 'w';
}

// gibt zurück, ob ein Schiff versenkt ist
bool SchiffSpieler::istVersenkt(int z, int s) {
	// wenn das Ursprungsfeld nicht versenkt ist (--> Großbuchstabe), kann direkt abgebrochen werden
	if (feldDaten[z][s] <= 'Z') {
		return false;
	}

	// in alle 4 Richtungen der Koordinanten solange laufen, bis entweder Wasser erreicht wird (--> alle Zellen versenkt) oder nicht versenkte Zelle
	// nach oben
	for (int i = z - 1; i >= 0; i--) {
		// wenn auf Wasser getroffen --> Loop wird abgebrochen
		if (istWasser(i, s)) {
			break;
		}

		// Das Ursprungsfeld ist bekannt versenkt --> Alle Felder
		if (feldDaten[i][s] != feldDaten[z][s]) {
			return false;
		}
	}

	// nach unten
	for (int i = z + 1; i < feldHoehe; i++) {
		// wenn auf Wasser getroffen --> Loop wird abgebrochen
		if (istWasser(i, s)) {
			break;
		}

		// Das Ursprungsfeld ist bekannt versenkt --> Alle Felder
		if (feldDaten[i][s] != feldDaten[z][s]) {
			return false;
		}
	}

	// nach rechts
	for (int i = s + 1; i < feldBreite; i++) {
		// wenn auf Wasser getroffen --> Loop wird abgebrochen
		if (istWasser(z, i)) {
			break;
		}

		// Das Ursprungsfeld ist bekannt versenkt --> Alle Felder
		if (feldDaten[z][i] != feldDaten[z][s]) {
			return false;
		}
	}

	// nach links
	for (int i = s - 1; i >= 0; i--) {
		// wenn auf Wasser getroffen --> Loop wird abgebrochen
		if (istWasser(z, i)) {
			break;
		}

		// Das Ursprungsfeld ist bekannt versenkt --> Alle Felder
		if (feldDaten[z][i] != feldDaten[z][s]) {
			return false;
		}
	}

	// Wenn bis hierhin gekommen, ist es ein Treffer
	return true;
}