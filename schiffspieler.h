/*
 * Dateiname:			schiffspieler.h
 * Datum:				2021-06-22 08:38:42
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-22 08:38:42
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 */

#pragma once

#include "resources.h"
using namespace boxDrawingChars;

#include <string>
using namespace std;

// Stellt einen Battleschipps-Spieler dar
class SchiffSpieler {
	public:
		/**
		 * Spielfeldgröße Variablen
		 */
		const int feldBreite = 10; // Anzahl Spielfeldzellen in der Breite (A --> H)
		const int feldHoehe = 10; // Anzahl Spielfeldzellen in der Höhe (1 --> 10)
		const int zelleBreite = 5; // Char-Breite einer einelnen Zelle
		const int zelleHoehe = 2; // Linien-Hoehe einer einzelnen Zelle
		const int visuelleHoehe = (feldHoehe + 1) * (zelleHoehe + 1) + 1; // Anzahl der visuell benötigten Zeilen für das komplette Spielfeld
		const int visuelleBreite = (feldBreite + 1) * (zelleBreite + 1) + 1; // Anzahl der visuell benötigten Zeichen für das komplette Spielfeld
		const int felderAbstand = 4; // Anzahl der Leerstelle zwischen den beiden Feldern
		
		/**
		 * Methoden
		 */
		SchiffSpieler();
		SchiffSpieler(const string name);
		SchiffSpieler(const string name, const string nameGenitiv);

		string getName();
		string getNameGenitiv();
		string * getEigenesSpielfeld();
		string * getEigenesSpielfeldSimple();
		string * getFeldDaten();
		string * getGegnerDaten();
		int * getAnzahlSchiffe();

		void spielfeldAusgeben(const string gegnerName);
		void spielfeldAusgeben(const string titel, const string gegnerName);
		void spielfeldAusgeben(const string titel, const string gegnerName, bool gebeGegnerAus);
		void meinSpielfeldAusgeben(const string titel);

		int typ2SchiffLaenge(char typ);
		char schiffLaenge2Typ(int len);
		string koord2String(const int * koordinaten);
		string koord2String(int z, int s);

		bool schiffSetzen(int z, int s, int orientierung, char typ);
		bool schiffSetzen(int z, int s, int orientierung, char typ, bool testen);
		int schiesseAufGegner(SchiffSpieler * gegner, int z, int s);
		void felderReset();

		void schiffePlatzieren();
		bool spielzugSingleplayer(SchiffSpieler * gegner, int * schuss);
		bool spielzugMultiplayer(SchiffSpieler * gegner, int * schuss);
		bool pruefeGewonnen(SchiffSpieler * gegner);
	private:
		/**
		 * Spielvariablen
		 */
		string spielerName;
		string spielerNameGenitiv;
		int anzahlSchiffe[4] = { 1, 2, 3, 4 }; // Anzahl der Schiffe; [0] = Schlachtschiff, [1] = Kreuzer, [2] = Zerstörer, [3] = U-Boot
		// int anzahlSchiffe[4] = { 0, 0, 0, 1 }; // DEBUGGING

		/**
		 * dynamische Erstellung nicht möglich, da ISO C++ nur eindiminensionale Arrays dynamisch erstellen kann
		 * Beide Arrays sind von der Größe [feldBreite][feldHoehe]
		 * 
		 * DICTIONARY: feldDaten
		 * W --> Wasser (kann nicht versenkt werden)
		 * S --> Schlachtschiff
		 * K --> Kreuzer
		 * Z --> Zerstörer
		 * U --> U-Boote
		 * Kleingeschriebene Schiff --> versenkte Version
		 * 
		 * DICTIONARY: gegnerDaten
		 * 0 --> noch nicht beschossen
		 * w --> kein Treffer
		 * x --> Treffer
		 */
		char feldDaten[10][10]; // stellt das momentane Spielfeld dar
		char gegnerDaten[10][10]; // stellt das Spielfeld des Gegners da

		/**
		 * Methoden
		 */
		void spielerInit(const string name, const string  nameGenitiv);

		string * erstelleSpielfeld(const char daten[10][10]);
		string erstelleReihe(const string bl, const string br, const string c1, const string c, const char * data);
		string erstelleReihe(const string bl, const string br, const string c1, const string c, const string s1, const string s);
		string erstelleCharReihe(const string bl, const string br, const string c1, const string c);
		string erstelleNumReihe(const string bl, const string br, const string c1, const string c, int n, const char * data);

		bool spielzug(SchiffSpieler * gegner, bool multiplayer, int * schuss);
		bool schiffTypPlatzieren(const string name, char typ, int anzahl);
		int zelleBeschiessen(int z, int s);
		
		bool pruefePlatz(int laenge);
		bool pruefeVerloren();

		string data2icon(char data);
		char data2char(char data);

		int * getValidKoordEingabe();

		bool istWasser(int z, int s);
		bool istVersenkt(int z, int s);
};
