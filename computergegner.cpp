/*
 * Dateiname:			computergegner.cpp
 * Datum:				2021-06-22 08:32:00
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-22 08:32:00
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include "computergegner.h"

#include <iostream>
#include <ctime>
#include <random>
using namespace std;

/**
 * public Methoden
 */
// Initialisiert den Computergegner mit dem Namen "Computer"
Computergegner::Computergegner() {
	initRandom();

	init("Computer");
}

// Initialisiert den Computergegner mit dem übergebenen Namen
Computergegner::Computergegner(const string computerName) {
	initRandom();

	init(computerName);
}


// gibt den Namen des Computers zurück
string Computergegner::getName() {
	return spieler->getName();
}

// gibt den Computergegner aus
SchiffSpieler * Computergegner::getSchiffSpieler() {
	return spieler;
}


// platziert die Schiffe des Computers
void Computergegner::schiffePlatzieren() {
	int * anzahlSchiffe = spieler->getAnzahlSchiffe();
	string * feldDaten;
	int size;
	int position; // die zufällig ausgewählte Position
	int positionCounter; // zählt die Anzahl der möglichen Positionen
	int positionen[10 * 10 * 4][3]; // speichert alle möglichen Positionen ab: Zeile, Spalte, Orientierung --> braucht vierfache Anzahl der Zellen an Platz

	// platziert die Schiffe der Größe nach absteigend
	for (int schiffCounter = 0; schiffCounter < 4; schiffCounter++) {
		size = 5 - schiffCounter;

		// platziere anzahlSchiffe[i] vom aktuellen Typ
		for (int n = 0; n < anzahlSchiffe[schiffCounter]; n++) {
			// positionen zurücksetzten
			positionCounter = 0;
			feldDaten = spieler->getFeldDaten();

			// Spielfeld ablaufen und alle möglichen Positionen zählen
			for (int z = 0; z < spieler->feldHoehe; z++) {
				for (int s = 0; s < spieler->feldHoehe; s++) {
					// überprüft alle 4 Richtungen
					for (int o = 0; o < 4; o++) {
						if (spieler->schiffSetzen(z, s, o, spieler->schiffLaenge2Typ(size), true)) {
							positionen[positionCounter][0] = z;
							positionen[positionCounter][1] = s;
							positionen[positionCounter][2] = o;
							positionCounter++;
						}
					}
				}
			}

			/**
			 * Wenn kein Platz mehr vorhanden ist, beendet die for-Schleife mit schiffCounter = 0
			 * Dann muss das platzieren neu gestartet werden. Hierzu wird die äußere Schleife zurückgesetzt,
			 * indem der Counter auf -1 gesetzt wird (nach Erreichen des Schleifenbefehlendes wird der counter um 1 erhöht (--> 0),
			 * bevor die Schleife erneut durchlaufen wird)
			 * Ebenso wird feldDaten zurückgesetzt / gelöscht
			 */
			if (positionCounter == 0) {
				schiffCounter = -1; // Größe der Schiffe-Schleife künstich neu starten
				n = anzahlSchiffe[schiffCounter]; // anzahl-Schiffe-Schleife künstlich beenden

				spieler->felderReset();
			} else {
				// wählt zufällig eine Position aus dem positionen-Array aus und platziert das Schiff dort
				position = rand() % positionCounter;

				spieler->schiffSetzen(positionen[position][0], positionen[position][1], positionen[position][2], spieler->schiffLaenge2Typ(size));
			}
		}
	}
}

// führt einen Spielzug aus. Gibt zurück, ob der Computer gewonnen hat
bool Computergegner::spielzug(SchiffSpieler * gegner, int * schuss) {
	int schussResult;
	bool schussDirAktiv = false;
	int schussDir = -1;
	int position;
	int koord[2] = { -1 }; // speichert die Koordinaten
	string * gegnerDaten = spieler->getGegnerDaten();

	/**
	 * Wenn alle schussDirs-Zellen false sind, gibt es kein aktiv beschossenes Schiff
	 * --> neuen Schuss nach Heatmap auswählen
	 */
	for (int i = 3; i >= 0; i--) {
		schussDirAktiv |= schussDirs[i];
	}
	
	if (!schussDirAktiv) { // schussDirs nicht aktiv --> Schießen nach heatmap
		// heatmapKumuliert für momentanes Spielsituation erstellen
		erstelleHeatmapKumuliert();

		position = rand() % heatmapKumuliert[9][9];

		// Feld ablaufen und die Zellen testen, ob position darin liegt
		for (int z = 0; z < 10; z++) {
			for (int s = 0; s < 10; s++) {
				if (position < heatmapKumuliert[z][s]) {
					// momentane Zelle, in Koordinaten-Array schreiben
					koord[0] = z;
					koord[1] = s; // s ist 1 zu niedrig

					// "Overflow" von koord[1] erkennen und korrigieren
					if (koord[1] == 10) {
						koord[0]++;
						koord[1]--;
					}

					// Schleife künstich vorzeitig beenden
					z = 10;
					s = 10;
				}
			}
		}
	} else { // schießen nach schussDir
		/**
		 * Wenn koord[0] noch den Initialisierungswert beinhaltet, wurden noch keine Koordinaten eingetragen
		 * --> weiter suchen
		 */
		while (koord[0] == -1) {
			/**
			 * schussDirs nach niedrigster Richtung untersuchen, in welche geschossen werden muss
			 */
			for (int i = 3; i >= 0; i--) {
				// speichert die Schussrichtung in schussDir
				if (schussDirs[i]) {
					schussDir = i;
				}
			}

			switch (schussDir) {
				// Osten
				case 0:
					for (int i = schussKoord[1] + 1; i < spieler->feldBreite; i++) {
						// wenn das Feld noch nicht beschossen wurde, diese Koordinaten verwenden
						if (gegnerDaten[schussKoord[0]][i] == '0') {
							koord[0] = schussKoord[0];
							koord[1] = i;

							// Schleife künstlich beenden
							i = spieler->feldBreite;
						}
					}
					break;
				// Süden
				case 1:
					for (int i = schussKoord[0] + 1; i < spieler->feldHoehe; i++) {
						// wenn das Feld noch nicht beschossen wurde, diese Koordinaten verwenden
						if (gegnerDaten[i][schussKoord[1]] == '0') {
							koord[0] = i;
							koord[1] = schussKoord[1];

							// Schleife künstlich beenden
							i = spieler->feldHoehe;
						}
					}
					break;
				// Westen
				case 2:
					for (int i = schussKoord[1] - 1; i >= 0; i--) {
						// wenn das Feld noch nicht beschossen wurde, diese Koordinaten verwenden
						if (gegnerDaten[schussKoord[0]][i] == '0') {
							koord[0] = schussKoord[0];
							koord[1] = i;

							// Schleife künstlich beenden
							i = -1;
						}
					}
					break;
				// Norden
				case 3:
					for (int i = schussKoord[0] - 1; i >= 0; i--) {
						// wenn das Feld noch nicht beschossen wurde, diese Koordinaten verwenden
						if (gegnerDaten[i][schussKoord[1]] == '0') {
							koord[0] = i;
							koord[1] = schussKoord[1];

							// Schleife künstlich beenden
							i = -1;
						}
					}
					break;
			}

			/**
			 * Wenn immer noch keine Koordinaten eingetragen sind, wurde in der bisherigen Richtung der Rand erreicht
			 * --> für zukünftige Durchläufe die Richtung deaktivieren
			 */
			if (koord[0] == -1) {
				schussDirs[schussDir] = false;
			}
		}
	}

	// Schuss ausführen
	schussResult = spieler->schiesseAufGegner(gegner, koord[0], koord[1]);

	// basierend auf der Schuss-Rückmeldung, heatmapAkt modifizieren
	switch (schussResult) {
		// Schuss ging ins Wasser. Wenn heatmapAkt (--> positionenCount == 0) leer ist, muss nichts unternommen werden, 
		case 0:
			// wenn heatmapAkt genutzt wird --> aktive Schuss-Richtung deaktivieren; in dieser Richtung wurde alles getroffen
			if (schussDirAktiv) {
				schussDirs[schussDir] = false;
			}
			break;
		case 1:
			if (!schussDirAktiv) {
				// Da erster Treffer (des Schiffs) --> Koordinaten speichern
				for (int i = 0; i < 2; i++) {
					schussKoord[i] = koord[i];
				}

				// alle 4 Schussrichtungen aktivieren
				for (int i = 0; i < 4; i++) {
					schussDirs[i] = true;
				}

				// wenn der Schusspunkt am Rand liegt, dementsprechende Richtungen deaktivieren
				// linker (rechter) Rand --> Westen (Osten) geht nicht
				if (koord[1] == 0) {
					schussDirs[2] = false;
				} else if (koord[1] == spieler->feldBreite - 1) {
					schussDirs[0] = false;
				}

				// oberer (unterer) Rand --> Norden (Süden) geht nicht
				if (koord[0] == 0) {
					schussDirs[3] = false;
				} else if (koord[0] == spieler->feldHoehe - 1) {
					schussDirs[1] = false;
				}

				// alle 4 Seiten überprüfen, ob diese bereits beschossen wurden --> != 0
				if (schussDirs[0] && gegnerDaten[koord[0]][koord[1] + 1] != '0') { // Osten
					for (int i = 0; i < 10; i++) {
						for (int j = 0; j < 10; j++) {
							printf("%c ", gegnerDaten[i][j]);
						}
						printf("\n");
					}
					
					schussDirs[0] = false;
				} else if (schussDirs[1] && gegnerDaten[koord[0] + 1][koord[1]] != '0') { // Süden
					schussDirs[1] = false;
				} else if (schussDirs[2] && gegnerDaten[koord[0]][koord[1] - 1] != '0') { // Westen
					schussDirs[2] = false;
				} else if (schussDirs[3] && gegnerDaten[koord[0] - 1][koord[1]] != '0') { // Norden
					schussDirs[3] = false;
				}
			} else { // es ist mindestens der zweite Treffer auf dieses Schiff
				if (schussKoord[0] == koord[0]) { // Das Schiff liegt Horizontal --> Es muss nicht nach Norden / Süden geschossen werden
					schussDirs[1] = false;
					schussDirs[3] = false;
				} else { // Schiff liegt vertikal
					schussDirs[2] = false;
					// wenn bereits vertikal geschossen wurde, ist Osten (schussDirs[0]) bereits deaktiviert
				}
			}
			break;
		// Schiff wurde versenkt, schussDirs wird deaktiviert
		case 2:
			for (int i = 0; i < 4; i++) {
				schussDirs[i] = false;
			}
			break;
	}

	// Schusskoordinaten und Ergebnis in "Rückgabe"-Array schreiben
	schuss[0] = koord[0];
	schuss[1] = koord[1];
	schuss[2] = schussResult;

	// dynamische Arrays löschen
	delete[] gegnerDaten;

	return spieler->pruefeGewonnen(gegner);
}


/**
 * private Methoden
 */
// gesammelte Funktion, die von den Konstruktoren aufgerufen wird
void Computergegner::init(const string computerName) {
	spieler = new SchiffSpieler(computerName);
}

// initilisiert srand mit clock()
void Computergegner::initRandom () {
	initRandom(clock());
}

// initialisiert srand mit start
void Computergegner::initRandom (int start) {
	srand(start); 
}


// erstellt die kumulierte Heatmap mit allen noch zu beschießenden Zellen
void Computergegner::erstelleHeatmapKumuliert() {
	string * gegnerDaten = spieler->getGegnerDaten(); // speichert das gegnerische Spielfeld

	// erstes heatmapKumuliert-Feld initialisieren. -1 --> wird wenn nicht andersweitig beschrieben, übergangen
	heatmapKumuliert[0][0] = -1;

	for (int z = 0; z < 10; z++) {
		for (int s = 0; s < 10; s++) {
			/**
			 * Wenn die Zelle noch nicht beschossen wurde, wird sie in das Array hinzugefügt
			 * (z + s) % 2 --> nur jede zweite Zelle muss getestet werden, da allle Schiffe mindestens 2 Zellen lang sind
			 */
			if (gegnerDaten[z][s] == '0' && (z + s) % 2 == 0) {
				// wenn es das erste Feld ist ([0][0]), gibt es keine vorherige Zelle, die addiert werden muss --> direkte Zuweisung
				if (z == 0 && s == 0) {
					heatmapKumuliert[z][s] = heatmap[z][s];
				} else {
					heatmapKumuliert[z][s] = heatmap[z][s] + heatmapKumuliert[z][s - 1];
				}
			} else { // wenn das Feld bereits beschossen wurde, den Wert vom vorherigen Feld übernehmen
				// Wenn es die erste Zelle gibt es keine vorherige Zelle zum übernehmen --> 0
				if (z == 0 && s == 0) {
					heatmapKumuliert[z][s] = 0;
				} else {
					heatmapKumuliert[z][s] = heatmapKumuliert[z][s - 1];
				}
			}
		}
	}

	// dynamische Arrays löschen
	delete[] gegnerDaten;
}