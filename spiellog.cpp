/*
 * Dateiname:			spiellog.cpp
 * Datum:				2021-06-25 05:15:42
 * Autor:				Simon Ziegler@skywalker (zisi1013@hs-karlsruhe.de)
 * 						Eugen Koch (koeu1011@h-ka.de)
 * Datum erstellt:		2021-06-25 05:15:42
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Zweck:				
 * Eingabe:				
 * Ausgabe:				
 */

#include "spiellog.h"

/**
 * Public-Methoden
 */
// Setzt den Dateinamen auf "Spielverlauf.txt"
Spiellog::Spiellog () {
	init("Spielverlauf.txt");
}

// Konstruktor mit custom-Dateiname
Spiellog::Spiellog (string _dateiname) {
	init(_dateiname);
}


// speichert die Spieler
void Spiellog::setzeSpieler(SchiffSpieler * _spieler1, SchiffSpieler * _spieler2, bool _multiplayer) {
	spieler1 = _spieler1;
	spieler2 = _spieler2;
	multiplayer = _multiplayer;

	// Spielernamen in Datei schreiben
	string message = "Spieler gegen ";

	if (multiplayer) {
		message += "Spieler";
	} else {
		message += "Computer";
	}

	message += "\n\n" + spieler1->getName() + " ist Spieler 1.\n";
	message += spieler2->getName() + " ist Spieler 2.\n";

	logSchreiben(message);
}


// schreibt die Startaufstellung in die Datei
void Spiellog::schreibeStartaufstellung() {
	string message = "\n" + spieler1->getNameGenitiv() + " Startaufstellung\n";
	schreibeSpielfeld(message, spieler1);

	message = "\n" + spieler2->getNameGenitiv() + " Startaufstellung\n";
	schreibeSpielfeld(message, spieler2);
	
	logSchreiben("\n");
}

// schreibt ein Spielfeld
void Spiellog::schreibeSpielfeld(const string message, SchiffSpieler * spieler) {
	logSchreiben(message);
	logSchreiben(spieler->getEigenesSpielfeldSimple(), spieler->feldHoehe + 1);
}

// schreibt den letzten Spielzug
void Spiellog::schreibeSpielzug (bool spieler, const int * schuss) {
	string message;

	switch (schuss[2]) {
		case 0:
			message = "Daneben!";
			break;
		case 1:
			message = "Treffer!";
			break;
		case 2:
			message = "Treffer versenkt!";
			break;
		default:
			message = "ERROR";
			break;
	}

	message = " Spielzug: Schuss auf " + spieler1->koord2String(schuss[0], schuss[1]) + ", " + message + '\n';

	if (spieler) {
		message = spieler2->getNameGenitiv() + message;
	} else {
		message = spieler1->getNameGenitiv() + message;
	}

	logSchreiben(message);
}

// schreibt den Gewinner und die End-Spielfelder
void Spiellog::schreibeGewinner(bool _gewinner) {
	string message;
	SchiffSpieler * gewinner;
	SchiffSpieler * verlierer;

	if (_gewinner) {
		gewinner = spieler2;
		verlierer = spieler1;
	} else {
		gewinner = spieler1;
		verlierer = spieler2;
	}

	message = "\n" + gewinner->getName() + spiel::win + gewinner->getNameGenitiv() + spiel::winSpielfeld + "\n";

	schreibeSpielfeld(message, gewinner);

	message = "\n" + verlierer->getNameGenitiv() + spiel::winSpielfeld + "\n";

	schreibeSpielfeld(message, verlierer);
}


/**
 * private-Methoden
 */
// initialisiert die Klasse
void Spiellog::init(string _dateiname) {
	dateiname = _dateiname;

	// Die alte Datei löschen, indem die Datei geöffnet und wieder geschlossen wird
	if (logOpenNoAppend()) {
		logClose();
	}
}


// öffnet die Logdatei im anhaengen-Modus
bool Spiellog::logOpen() {
	return logOpen(true);
}

// öffnet den Log
bool Spiellog::logOpen(bool anhaengen) {
	if (anhaengen) {
		logDatei.open(dateiname, ios::app);
	} else {
		logDatei.open(dateiname);
	}

	// Fehler beim Dateiöffnen abfragen
	if (!logDatei) {
		return false;
	} else {
		return true;
	}
}

// öffnet den Log im Überschreiben-Modus
bool Spiellog::logOpenNoAppend() {
	return logOpen(false);
}

// schließt die Logdatei
void Spiellog::logClose() {
	logDatei.close();
}


// schreibt einen String in die Datei
void Spiellog::logSchreiben(string message) {
	/**
	 * Wenn die Datei erfolgreich geöffnet wurde
	 * --> Schreiben und wieder schließen
	 * Ansonsten: nichts unternehmen
	 */
	if (logOpen()) {
		logDatei << message;

		logClose();
	}
}

// schreibt ein String-Array in die Datei
void Spiellog::logSchreiben(const string * message, int laenge) {
	/**
	 * Wenn die Datei erfolgreich geöffnet wurde
	 * --> Schreiben und wieder schließen
	 * Ansonsten: nichts unternehmen
	 */
	if (logOpen()) {
		for (int i = 0; i < laenge; i++) {
			logDatei << message[i] << endl;
		}

		logClose();
	}

	// dynamische Arrays löschen
	delete[] message;
}